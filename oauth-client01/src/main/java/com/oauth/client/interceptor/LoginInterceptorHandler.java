package com.oauth.client.interceptor;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;

/**
 * Created by Administrator on 2019/9/17.
 */
//@Component
public class LoginInterceptorHandler extends HandlerInterceptorAdapter{

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        // 获取用户
        Principal userPrincipal = request.getUserPrincipal();
        if (null == userPrincipal) {
            response.setStatus(401);
            return false;
        }

        return true;
    }
}
