/*
SQLyog 企业版 - MySQL GUI v8.14 
MySQL - 5.7.20 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;

CREATE TABLE `oauth_client_details` (
   `client_id` varchar(128) NOT NULL,
   `client_secret` varchar(256) NOT NULL,
   `resource_ids` varchar(256) DEFAULT NULL,
   `scope` varchar(1024) DEFAULT NULL,
   `authorized_grant_types` varchar(256) DEFAULT NULL,
   `web_server_redirect_uri` varchar(256) DEFAULT NULL,
   `authorities` varchar(2048) DEFAULT NULL,
   `access_token_validity` int(11) DEFAULT NULL,
   `refresh_token_validity` int(11) DEFAULT NULL,
   `additional_information` varchar(4096) DEFAULT NULL,
   `autoapprove` varchar(256) DEFAULT NULL,
   PRIMARY KEY (`client_id`)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='oauth2客户端信息'

-- 添加一个客户端
insert into `oauth_client_details` (`client_id`, `client_secret`, `resource_ids`, `scope`, `authorized_grant_types`, `web_server_redirect_uri`, `authorities`, `access_token_validity`, `refresh_token_validity`, `additional_information`, `autoapprove`) values('B5CDC04D8D8D419DA406364168F276A2','E2DA5B2DEDD548AEB7ABA689436C2E2C','','userProfile','authorization_code,client_credentials,password','http://localhost:9101/client1/login,http://www.baidu.com','','43200','2592000','','');

CREATE TABLE `oauth_access_token` (
   `token_id` varchar(256) DEFAULT NULL,
   `token` blob,
   `authentication_id` varchar(128) NOT NULL,
   `user_name` varchar(256) DEFAULT NULL,
   `client_id` varchar(256) DEFAULT NULL,
   `authentication` blob,
   `refresh_token` varchar(256) DEFAULT NULL,
   PRIMARY KEY (`authentication_id`)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='oauth2访问令牌'


CREATE TABLE `oauth_code` (
   `code` varchar(256) DEFAULT NULL,
   `authentication` blob,
   `create_ts` timestamp NULL DEFAULT CURRENT_TIMESTAMP
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='oauth2授权码'

CREATE TABLE `oauth_approvals` (
   `userId` varchar(256) DEFAULT NULL,
   `clientId` varchar(256) DEFAULT NULL,
   `scope` varchar(256) DEFAULT NULL,
   `status` varchar(10) DEFAULT NULL,
   `expiresAt` datetime DEFAULT NULL,
   `lastModifiedAt` datetime DEFAULT NULL
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='oauth2已授权客户端'


insert into `oauth_client_details` (`client_id`, `client_secret`, `resource_ids`, `scope`, `authorized_grant_types`, `web_server_redirect_uri`, `authorities`, `access_token_validity`, `refresh_token_validity`, `additional_information`, `autoapprove`) values('7gBZcbsC7kLIWCdELIl8nxcs','$2a$10$4di0sSQdr9yk4uTKWtZqzedsxI8sWXoR67x.G.Qmy4K2L7ZaFQt6W','','userProfile','authorization_code,client_credentials,password','http://localhost:2222/login,http://localhost:8888/login,http://localhost:8888/webjars/springfox-swagger-ui/o2c.html,http://www.baidu.com,http://localhost:9101/client1/login,http://localhost:9101/client1','','43200','2592000','{\"website\":\"http://www.baidu.com\",\"apiKey\":\"7gBZcbsC7kLIWCdELIl8nxcs\",\"secretKey\":\"0osTIhce7uPvDKHz6aa67bhCukaKoYl4\",\"appName\":\"平台用户认证服务器\",\"updateTime\":1562841065000,\"isPersist\":1,\"appOs\":\"\",\"appIcon\":\"\",\"developerId\":0,\"createTime\":1542016125000,\"appType\":\"server\",\"appDesc\":\"资源服务器\",\"appId\":\"1552274783265\",\"appNameEn\":\"open-cloud-uaa-admin-server\",\"status\":1}','');
insert into `oauth_client_details` (`client_id`, `client_secret`, `resource_ids`, `scope`, `authorized_grant_types`, `web_server_redirect_uri`, `authorities`, `access_token_validity`, `refresh_token_validity`, `additional_information`, `autoapprove`) values('B5CDC04D8D8D419DA406364168F276A2','E2DA5B2DEDD548AEB7ABA689436C2E2C','','userProfile','authorization_code,client_credentials,password','http://localhost:9101/client1/login,http://baidu.com,http://www.baidu.com','','43200','2592000','{\"website\":\"http://www.baidu.com\",\"apiKey\":\"7gBZcbsC7kLIWCdELIl8nxcs\",\"secretKey\":\"0osTIhce7uPvDKHz6aa67bhCukaKoYl4\",\"appName\":\"平台用户认证服务器\",\"updateTime\":1562841065000,\"isPersist\":1,\"appOs\":\"\",\"appIcon\":\"\",\"developerId\":0,\"createTime\":1542016125000,\"appType\":\"server\",\"appDesc\":\"资源服务器\",\"appId\":\"1552274783265\",\"appNameEn\":\"open-cloud-uaa-admin-server\",\"status\":1}','');
