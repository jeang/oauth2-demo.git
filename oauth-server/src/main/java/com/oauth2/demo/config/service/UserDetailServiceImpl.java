package com.oauth2.demo.config.service;

import com.oauth2.demo.mapper.entity.UserInfoEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * Created by Administrator on 2019/9/8.
 */
@Configuration
public class UserDetailServiceImpl implements UserDetailsService{

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        UserInfoEntity userInfoEntity = new UserInfoEntity();
        userInfoEntity.setUsername(s);
        userInfoEntity.setPassword(passwordEncoder.encode("111111"));
        return userInfoEntity;
    }

    
}
