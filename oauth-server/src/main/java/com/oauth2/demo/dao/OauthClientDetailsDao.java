package com.oauth2.demo.dao;


import com.oauth2.demo.mapper.BaseMapper;
import com.oauth2.demo.mapper.entity.OauthClientDetails;

/**
 * Created by Administrator on 2019/9/6.
 */
public interface OauthClientDetailsDao extends BaseMapper<OauthClientDetails> {
}
